package com.example.manager;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.authentication.LoginResponse;

@Repository
public interface ManagerRepository extends JpaRepository<ManagerModel, Integer> {
	
	@Query("SELECT new com.solv.cm.v1.authentication.LoginResponse(m.managerId, m.emailId, m.password) FROM ManagerModel m where m.email =:email AND m.password =:password")
	List<LoginResponse> findByEmailIdAndPassword(@Param("email") String email, @Param("password") String password);
	

}
