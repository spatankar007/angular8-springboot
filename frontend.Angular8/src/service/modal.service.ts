import { SweetAlertComponent } from '../app/common/sweet-alert/sweet-alert.component';
import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class SweetModalService {
  constructor(private modalService: BsModalService) { }
  modalRef: BsModalRef;
  openPopupMessage(message, title) {
    const initialState = {
      message: message, title: title
    };
    return this.modalService.show(SweetAlertComponent, Object.assign({}, { class: 'modal-dialog-centered sweet-alert', ignoreBackdropClick: true, initialState })).content.action;
  }
}
