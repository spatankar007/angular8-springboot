import { ToastrService } from 'ngx-toastr';
import { UtilService } from './util.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserRoleGuard implements CanActivate {
  userType: any;
  error: any = ""
  constructor(
    public utilService: UtilService,
    private toaster: ToastrService
  ) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let allowedRoles = next.data;
    allowedRoles = allowedRoles.allowedRoles;
    if (localStorage.getItem('X-Auth-Token') && this.utilService.getLocal(['X-Auth-Role-Name'])) {
      for (let i = 0; i < allowedRoles.length; i++) {
        if (allowedRoles[i] == this.utilService.getLocal(['X-Auth-Role-Name'])['X-Auth-Role-Name']) {
          return true;
        }
      }
      this.utilService.locationBack();
      return false;
    } else {
      this.utilService.goToLogin();
      return false;
    }
  }
}
