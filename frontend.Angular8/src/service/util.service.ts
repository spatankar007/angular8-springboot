import { Router } from '@angular/router';
import { MathUtilService } from './mathUtil.service';
import * as moment from "moment";
import * as CryptoJS from 'crypto-js';
import { environment } from '../environments/environment';
import { ConstantValues } from './constants';
import { Location } from "@angular/common";
import { ToastrService } from "ngx-toastr";
export class UtilService {
  constructor(
    private router: Router,
    private toastrService: ToastrService,
    private location: Location,
    private constantValues: ConstantValues,
  ) {

  }
  static jsonToUrlEncode(data) {
    return Object.keys(data).map((key) => {
      return encodeURIComponent(key)
        + '=' + encodeURIComponent(data[key]);
    }).join('&')
  }

  goToLogin() {
    // localStorage.clear();
    return this.router.navigate(["/login"]);
  }

  locationBack() {
    return this.location.back();
  }

  static copyToClipBoard(dataToCopy) {
    document.addEventListener('copy', (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', (dataToCopy));
      e.preventDefault();
      document.removeEventListener('copy', null);
    });
    document.execCommand('copy');
  }

  jsonLength(json) {
    return Object.keys(json).length;
  }

  compareValues(key, order = "desc") {
    return function innerSort(a, b) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        return 0;
      }

      const varA = typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
      const varB = typeof b[key] === "string" ? b[key].toUpperCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return order === "desc" ? comparison * -1 : comparison;
    };
  }

  static replaceSpaceAmpercentSymbolWithDash(str) {
    return str.replace(/ & |\s+/g, "-").toLowerCase();
  }

  static titleCase(data: string) {
    return data.split(' ').map(function (val) {
      return val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
    }).join(' ');
  }

  static jsonToArray(json) {
    var result = [];
    for (let i in json) {
      result.push(json[i])
    }
    return result
  }

  prettifyToString(obj: any) {
    return JSON.stringify(obj, null, 4);
  }

  checkDateValid(date: any) {
    return moment(new Date(date)).isValid();
  }

  isNumber(obj: any) {
    return isNaN(obj);
  }

  groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }

  typeOf(string) {
    return typeof string;
  }

  static calulateDsAfterPr(unitPrice: number, pr: number) {
    const multiply = MathUtilService.safeMultiply(unitPrice, pr);
    const divide = MathUtilService.safeDivide(multiply, 100);
    const subtract = MathUtilService.safeSubtract(unitPrice, divide);
    return subtract;
  }

  static pricePercentageCalculation(event: any, unitPrice: number, xIndex: number, yIndex: number, array: Array<any>) {
    event = isNaN(event) ? "" : Math.abs(event);
    if (array && array[xIndex]["discountDivision"] && array[xIndex]["discountDivision"][yIndex]) {
      array[xIndex]["discountDivision"][yIndex]["percentage"] = event || 0;
      unitPrice = +unitPrice;
      if (array[xIndex]["discountDivision"].length) {
        for (var i = yIndex - 1; i >= 0; i--) {
          if (typeof array[xIndex]["discountDivision"][i]["price"] == "object") {
            unitPrice = array[xIndex]["discountDivision"][i]["price"];
            break;
          }
        }
      }
      if (
        array[xIndex]["discountDivision"].length &&
        !(yIndex + 1 == array[xIndex]["discountDivision"].length) &&
        array[xIndex]["discountDivision"][yIndex + 1]["price"]
      ) {
        for (var i = yIndex; i < array[xIndex]["discountDivision"].length; i++) {
          if (typeof array[xIndex]["discountDivision"][i]["price"] == "object") {
            const ModifiedCalculation: any = UtilService.calulateDsAfterPr(
              array[xIndex]["discountDivision"][i - 1] ? array[xIndex]["discountDivision"][i - 1]["price"] : unitPrice,
              array[xIndex]["discountDivision"][i]["percentage"]
            );
            array[xIndex]["discountDivision"][i]["price"] = isNaN(ModifiedCalculation)
              ? UtilService.calulateDsAfterPr(
                array[xIndex]["discountDivision"][i]["price"],
                array[xIndex]["discountDivision"][i]["percentage"]
              )
              : ModifiedCalculation;
          }
        }
      }
      const finalCalculation: any = UtilService.calulateDsAfterPr(
        unitPrice,
        event
      );
      array[xIndex]["discountDivision"][yIndex]["price"] = isNaN(finalCalculation)
        ? ""
        : finalCalculation;
    }

    return array;
  }

  static tableHeaderCalculation(array: Array<any>, info = false) {
    //cleanup everything
    let largest = 0;
    if (info) {
      if (array["items"].length) {
        array["items"].map((item, i) => {
          if (item.discountDivision && item.discountDivision.length) {
            item.discountDivision.map((_subItem: any, j: any) => {
              if (array["items"][i].discountDivision.length > largest) {
                largest = array["items"][i].discountDivision.length;
              }
            });
          }
        });
      }
      if (array["items"].length) {
        array["items"].map((item, _index) => {
          if (item.discountDivision.length < largest) {
            for (let i = item.discountDivision.length; i < largest; i++) {
              item.discountDivision.push({
                percentage: '--',
                price: '--',
                year: '--',
                tn: '--'
              });
            }
          }
        });
      }
    } else {
      if (array.length) {
        array.map((item, i) => {
          if (item.discountDivision && item.discountDivision.length) {
            const endTenure = array[i]["tenure"]
            item.discountDivision.map((subItem, j) => {
              array[i]["discountDivision"] = array[i]["discountDivision"].filter(function (el) {
                return (!isNaN(el.tn) && el.tn <= endTenure);
              });
            });
            // remove duplicates
            array[i]["discountDivision"] = array[i]["discountDivision"].filter((thing, index) => {
              return index === array[i]["discountDivision"].findIndex(obj => {
                return obj.year === thing.year;
              });
            });
          }
        });
      }
      if (array.length) {
        array.map((item, i) => {
          if (item.discountDivision && item.discountDivision.length) {
            item.discountDivision.map((subItem, j) => {
              if (array[i]["discountDivision"].length > largest) {
                largest = array[i]["discountDivision"].length;
              }
            });
          }
        });
      }
      if (array.length) {
        array.map((item, index) => {
          if (item.discountDivision) {
            if (item.discountDivision.length < largest) {
              for (let i = item.discountDivision.length; i < largest; i++) {
                item.discountDivision.push({
                  percentage: '--',
                  price: '--',
                  year: '--',
                  tn: '--'
                });
              }
            }
          }
        });
      }
    }

    return {
      array,
      largest
    }
  }

  static financalYearCalculation(start: any, end: any) {
    let startMonth = start.month() + 1
    let nextMarch = startMonth >= 4 ? start.year() + 1 : start.year()
    nextMarch = moment('31-03-' + nextMarch, 'DD-MM-YYYY')
    let diff = Math.ceil(end.diff(nextMarch, 'year', true))
    return diff <= 0 ? 1 : diff + 1;
  }

  static tenureCalculation(start: any, end: any, itemIndex: number, array: Array<any>) {
    start = moment(start)
    end = moment(end)
    let tenure = this.financalYearCalculation(start, end);
    if (!isNaN(tenure)) {
      array[itemIndex]["tenure"] = `${tenure}`;

      const startInfo = (new Date(start)).getFullYear()

      if (!array[itemIndex]["discountDivision"]) {
        array[itemIndex]["discountDivision"] = [];
      } else {
        array[itemIndex]["discountDivision"] = array[itemIndex]["discountDivision"].filter(function (el) {
          return (!isNaN(el.tn) && parseInt(el.tn) <= tenure);
        });
      }

      if (tenure > 1) {
        for (let i = 2; i <= tenure; i++) {
          array[itemIndex]["discountDivision"].push({
            percentage: "",
            price: "",
            year: (startInfo + i - 1).toString(),
            tn: i
          });
        }

        // remove duplicates
        array[itemIndex]["discountDivision"] = array[itemIndex]["discountDivision"].filter((thing, index) => {
          return index === array[itemIndex]["discountDivision"].findIndex(obj => {
            return obj.year === thing.year;
          });
        });
      }
    }

    return array
  }

  static onlyTenureCalculation(itemIndex: number, array: Array<any>, tenure?: number) {
    tenure = tenure < 0 ? 0 : Math.ceil(tenure);
    if (!isNaN(tenure)) {
      array[itemIndex]["tenure"] = `${tenure}`;

      const startInfo = (new Date()).getFullYear()

      if (!array[itemIndex]["discountDivision"]) {
        array[itemIndex]["discountDivision"] = [];
      } else {
        array[itemIndex]["discountDivision"] = array[itemIndex]["discountDivision"].filter(function (el) {
          return (!isNaN(el.tn) && parseInt(el.tn) <= tenure);
        });
      }

      if (tenure > 1) {
        for (let i = 2; i <= tenure; i++) {
          array[itemIndex]["discountDivision"].push({
            percentage: "",
            price: "",
            year: (startInfo + i - 1).toString(),
            tn: i
          });
        }

        // remove duplicates
        array[itemIndex]["discountDivision"] = array[itemIndex]["discountDivision"].filter((thing, index) => {
          return index === array[itemIndex]["discountDivision"].findIndex(obj => {
            return obj.year === thing.year;
          });
        });
      }
    }
    return array
  }

  static downLoadCSV(data: any, fileName: string) {

    const blob = new Blob([data], { type: 'text/csv;charset=utf-8;' });

    const link = document.createElement('a');
    if (link.download !== undefined) {
      const url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  static downLoadFile(data: any, type: string, fileName) {
    let blob = new Blob([data], { type: type });
    let url = window.URL.createObjectURL(blob);
    let a = document.createElement("a");
    a.href = url;
    a.download = fileName;
    a.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(url);
    }, 0)
  }

  getDifference(theDate: string): string {
    let end = moment(moment(moment()).valueOf());
    let updatedAt = moment(moment(theDate).valueOf());
    let diff = end.diff(updatedAt, "hour", false);
    if (diff > 8760) {
      diff = end.diff(updatedAt, "years", false);
      return diff > 1 ? diff + " years ago" : diff + " year ago";
    } else if (diff > 730) {
      diff = end.diff(updatedAt, "months", false);
      return diff > 1 ? diff + " months ago" : diff + " month ago";
    }

    else if (diff > 24) {
      diff = end.diff(updatedAt, "days", false);
      return diff > 1 ? diff + " days ago" : diff + " day ago";
    } else if (diff <= 0) {
      diff = end.diff(updatedAt, "minutes", false);
      return diff > 1 ? diff + " minutes ago" : diff + " minute ago";
    } else return diff > 1 ? diff + " hours ago" : diff + " hour ago";
  }

  removeUnwantedDecimal(event) {
    let splitValue = event.target.value.split('.')
    if (splitValue[1].length == 0) {
      event.target.value = event.target.value.replace(/\./g, '')
    }
  }
  withDecimalNumberKey(event, type) {
    if (event.key >= 0 || event.key <= 9 || event.key == "." || event.key == "Backspace") {
      let value;
      let match;
      match = event.target.value.split('.')
      if (match.length >= 2 && match[1].length >= 1) {
        if (match[1].length >= 2) {
          match[1] = match[1].slice(0, 2);
        } else {
          match[1] = match[1];
        }
        value = match[0] + "." + match[1];
        event.target.value = value;
        return false;
      }
    } else {
      event.target.value = event.target.value.slice(0, -1);
      return false;
    }

  }

  setLocal(data: object) {
    for (const [key, value] of Object.entries(data)) {
      localStorage.setItem(key, this.encryption(value));
    }
  }

  getLocal(data: Array<string>) {
    let localData: object = {};
    for (const value of data) {
      localData[value] = this.decryption(localStorage.getItem(value));
    }
    if (Object.keys(localData).length > 0) return localData;
    return false;
  }

  encryption(data: string) {
    const secret = environment['secretKey'];
    return CryptoJS.AES.encrypt(data, secret).toString();
  }

  decryption(data: string) {
    const secret = environment['secretKey'];
    return CryptoJS.AES.decrypt(data, secret).toString(CryptoJS.enc.Utf8);
  }

  getFileNameFromUrl(url) {
    if (url) {
      return url.split("/").pop();
    }
  }

  getFileExtension(url) {
    if (url) {
      return this.getFileNameFromUrl(url)
        .split(".")
        .pop();
    }
  }

  static mergeTwoObjects(oldObject, newObject) {
    return { ...oldObject, ...newObject };
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  parseIntUtil(number) {
    return parseFloat(number);
  }

  fileType(fileName) {
    let ext = this.constantValues.allowedExtension;
    let extension;
    extension = this.getFileExtension(fileName);
    if (extension && ext.filter(i => i == extension).length) {
      return true;
    } else {
      this.toastrService.error(
        "All the documents should be doc format, pdf format, image format",
        "File Type"
      );
      return false;
    }
  }
}
