package com.example.manager;

import java.util.List;

import com.example.authentication.LoginResponse;

public interface ManagerService {

	ManagerModel save(ManagerModel managerModel);

	List<LoginResponse> findByEmailIdAndPassword(String emailId, String password);

}
