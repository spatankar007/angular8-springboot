package com.example.authentication;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.manager.ManagerModel;
import com.example.manager.ManagerService;
import com.example.util.LoginStatus;
import com.example.util.Masking;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@CrossOrigin(origins = "*")
@Transactional
@RestController
@RequestMapping(path = "/authentication")
public class AuthenticationController {

	private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	@Value("${jwt.secret}")
	private static String secret;

	@Autowired
	ManagerService managerService;

	@PostMapping("/login")
	public ResponseEntity<LoginStatus> getUserName(@RequestBody ManagerModel managerModel) throws Exception {
		LoginStatus loginStatus = new LoginStatus();
		String jwtToken = "";
		try {
			List<LoginResponse> peopleList = (List<LoginResponse>) managerService
					.findByEmailIdAndPassword(managerModel.getEmailId(), managerModel.getPassword());

			if (peopleList != null) {

				for (int i = 0; i < peopleList.size(); i++) {
					String managerId = peopleList.get(i).getManagerId().toString();
					String emailId = peopleList.get(i).getEmailId();
					String password = peopleList.get(i).getPassword();
					String decryptedPassword = Masking.decrypt(password, secret);
					jwtToken = createJWT(emailId, managerId);
					if (emailId.equals(managerModel.getEmailId())
							
							&& decryptedPassword.equals(managerModel.getPassword())) {

						loginStatus.setManagerId(managerId);
						loginStatus.setJwtToken(jwtToken);
						loginStatus.setSuccess(true);
						loginStatus.setMessage("Login Success");
						System.out.println("Jwt token " + jwtToken);

					} else {
						loginStatus.setMessage("Email/Password did not match");
						loginStatus.setSuccess(false);
						return new ResponseEntity<>(loginStatus, HttpStatus.UNAUTHORIZED);

					}

				}

				return new ResponseEntity<>(loginStatus, HttpStatus.ACCEPTED);

			} else {
				loginStatus.setMessage("Email/Password does not exists");
				loginStatus.setSuccess(false);
				return new ResponseEntity<>(loginStatus, HttpStatus.UNAUTHORIZED);

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(loginStatus, HttpStatus.BAD_REQUEST);

		}

	}

	private static String createJWT(String userName, String role) throws Exception {

		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.DATE, 1);

		System.out.println("date:" + cal.getTime());
		logger.info("date: " + cal);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary("secret");
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(userName).setIssuedAt(now).setIssuer(role)
				.signWith(signatureAlgorithm, signingKey); // .setExpiration(cal.getTime()

		return builder.compact();

	}

}
