import { LoginComponent } from '../login/login.component';
import { PageLoaderComponent } from '../../common/page-loader/page-loader.component';
import { PopupModalComponent } from '../../common/popup-modal/popup-modal.component';
import { FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpsService } from '../../../service/https.service';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from '../../../module/material-module';


describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    const formBuilder: FormBuilder = new FormBuilder();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LoginComponent,
                PageLoaderComponent,
                PopupModalComponent
            ],
            imports: [
                ReactiveFormsModule,
                MaterialModule,
                NgxSpinnerModule,
                HttpClientModule,
                ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                RouterModule.forRoot([])
            ],
            providers: [
                {
                    provide: FormBuilder,
                    useValue: formBuilder
                },
                HttpsService
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        component.loginForm = formBuilder.group({
            email: ['idrees.tarwadi@acc.ltd', [Validators.required, Validators.email]],
            password: ['123', Validators.required]
            //type: ['Customer', Validators.required]
            
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});