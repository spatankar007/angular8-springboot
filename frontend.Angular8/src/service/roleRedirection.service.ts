import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UtilService } from './util.service';

@Injectable({
  providedIn: 'root'
})
export class RoleRedirectionService {
  constructor(
    private router: Router
  ) { }

  loginRedirection(role) {
    return this.router.navigate([UtilService.replaceSpaceAmpercentSymbolWithDash(role)]);
  }


}
