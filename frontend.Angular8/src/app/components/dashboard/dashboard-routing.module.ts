import { ProfileComponent } from './profile/profile.component';
import { CalendarComponent } from './calender/calendar.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InboxComponent } from './inbox/inbox.component';
//leaves-routing.module.ts
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { TableComponent } from './table/table.component';
import { CustomerComponent } from './customer/customer.component';
import { ChatroomComponent } from './chatroom/chatroom.component';
import { HelpcenterComponent } from './helpcenter/helpcenter.component';
import { SettingsComponent } from './settings/settings.component';


const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      {
        path: 'dashboard', component: HomeComponent
      },
      {
        path: 'table', component: TableComponent
      },
      {
        path: 'inbox', component: InboxComponent
      },
      {
        path: 'invoice', component: InvoiceComponent
      },
      {
        path: 'customer', component: CustomerComponent
      },
      {
        path: 'chatroom', component: ChatroomComponent
      },
      {
        path: 'calender', component: CalendarComponent
      },
      {
        path: 'helpcenter', component: HelpcenterComponent
      },
      {
        path: 'setting', component: SettingsComponent
      },
      {
        path: 'profile', component: ProfileComponent
      },
      {
        path: '**', redirectTo: 'dashboard'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
