import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
/* Plugins */

import { ChartsModule } from 'ng2-charts';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule, DatepickerModule, ModalModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

/* Component */
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonModulesModule } from './common/common-modules.module';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { SweetAlertComponent } from './common/sweet-alert/sweet-alert.component';

/* Service */
import { UtilService } from '../service/util.service';
import { HttpsService } from '../service/https.service';
import { DataService } from '../service/data.service';
import { SweetModalService } from './../service/modal.service';
import { JsonToCsv } from '../service/jsonToCsv';
import { RoleRedirectionService } from '../service/roleRedirection.service';

/* Module */
import { MaterialModule } from '../module/material-module';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    CommonModulesModule,
    ChartsModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [
    DataService,
    HttpsService,
    UtilService,
    JsonToCsv,
    RoleRedirectionService,
    SweetModalService
  ],
  bootstrap: [AppComponent],
  entryComponents: [SweetAlertComponent]
})
export class AppModule { }
