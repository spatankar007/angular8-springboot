package com.example.manager;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.authentication.LoginResponse;

@Service
public class ManagerServiceImpl implements ManagerService {
	
	@Autowired
	ManagerRepository managerRepository;

	@Override
	public ManagerModel save(ManagerModel managerModel) {
		return managerRepository.save(managerModel);
	}

	@Override
	public List<LoginResponse> findByEmailIdAndPassword(String emailId, String password) {
		return managerRepository.findByEmailIdAndPassword(emailId, password);
	}

}
