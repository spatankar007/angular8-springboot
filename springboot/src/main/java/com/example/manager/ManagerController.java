package com.example.manager;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.util.StatusMessage;

@CrossOrigin(origins = "*")
@Transactional
@RestController
@RequestMapping(path = "/manager")
public class ManagerController {

	private static final Logger logger = LoggerFactory.getLogger(ManagerController.class);

	@Autowired
	private ManagerService managerService;

	@Autowired
	public ManagerController(ManagerService managerService) {
		this.managerService = managerService;

	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/register")
	public ResponseEntity<StatusMessage> saveManager(@RequestBody ManagerModel managerModel) {
		StatusMessage statusMessage = new StatusMessage();

		try {
			ManagerModel managerModelResponse = managerService.save(managerModel);
			if (managerModelResponse != null) {
				statusMessage.setMessage("Manager Details Captured");
				statusMessage.setSuccess(true);
				return new ResponseEntity<>(statusMessage, HttpStatus.CREATED);
			} else {
				// return error message
				statusMessage.setMessage("Failed");
				statusMessage.setSuccess(false);
				return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			statusMessage.setMessage(e.getMessage());
			statusMessage.setSuccess(false);
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
		}
	}

}
