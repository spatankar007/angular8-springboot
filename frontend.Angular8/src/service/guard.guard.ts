import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private toaster: ToastrService) {

  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    const redirectUrl = route['_routerState']['url'];
    if (!localStorage.getItem('X-Auth-Token')) {
      this.router.navigate(['/login']);
      this.toaster.warning("Please Login First");
      return false;
    }
    return true;
    // throw new Error("Method not implemented.");
  }

}
