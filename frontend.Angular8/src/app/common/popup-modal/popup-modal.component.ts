// mymodal.component.ts
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../../../service/data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-modal',
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.scss']
})
export class PopupModalComponent implements OnInit {
  @ViewChild('mymodal', { static: false }) mymodal;
  @ViewChild('spinnerLoad', { static: false }) spinnerLoad;

  fakeData: any = []

  constructor(
    public modalService: NgbModal,
    public toastrService: ToastrService,
    private dataService: DataService
  ) { }

  ngOnInit() { }

  open(content) {
    this.modalService.open(this.mymodal, { centered: true })
      .result.then((result) => {
        console.log(`Closed with: ${result}`)
      }, (reason) => {
        console.log(`Dismissed ${reason}`)
      });

    this.spinnerLoad.spinnerShow();
    this.dataService.fakeGet(1).subscribe(res => {
      this.spinnerLoad.spinnerHide();
      this.fakeData = res
    }, err => {
      this.spinnerLoad.spinnerHide();
      this.toastrService.error(err['message'])
    });
  }

}
