import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export function Animation(rightDuration: number,leftDuration: number) {
    return trigger('slideRight', [
        state(
            'left',
            style({
                transform: 'translateX(0vh)'
            })
        ),
        state(
            'right',
            style({
                transform: 'translateX(500vh)'
            })
        ),
        transition(
            'right => left',
            animate(
                `${rightDuration}s ease`,
                keyframes([
                    style({
                        transform: 'translateX(400vh)',
                        background: 'transparent',
                        offset: 0
                    }),
                    style({
                        transform: 'translateX(300vh)',
                        background: 'transparent',
                        offset: 0.25
                    }),
                    style({
                        transform: 'translateX(200vh)',
                        background: 'transparent',
                        offset: 0.5
                    }),
                    style({
                        transform: 'translateX(100vh)',
                        background: 'transparent',
                        offset: 0.75
                    }),
                    style({
                        transform: 'translateX(0vh)',
                        background: 'transparent',
                        offset: 1
                    })
                ])
            )
        ),

        transition(
            'left => right',
            animate(
                '1.5s ease',
                keyframes([
                    style({
                        transform: 'translateX(0vh)',
                        background: 'transparent',
                        offset: 0
                    }),
                    style({
                        transform: 'translateX(100vh)',
                        background: 'transparent',
                        offset: 0.25
                    }),
                    style({
                        transform: 'translateX(200vh)',
                        background: 'transparent',
                        offset: 0.5
                    }),
                    style({
                        transform: 'translateX(300vh)',
                        background: 'transparent',
                        offset: 0.75
                    }),
                    style({
                        transform: 'translateX(400vh)',
                        background: 'transparent',
                        offset: 1
                    })
                ])
            )
        )
    ])
}
