package com.example.util;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONException;
import org.json.JSONObject;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JWTokenVerify {
	public static JSONObject verifyJWT(String token, String jwtSecret) throws JSONException {

		JSONObject response = new JSONObject();
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(jwtSecret))
					.parseClaimsJws(token).getBody();

			if (claims != null && !(claims.isEmpty())) {
				response.put("claims", claims);
				response.put("message", "valid");
				response.put("statusCode", 200);
			} else {
				response.put("claims", claims);
				response.put("message", "invalid");
				response.put("statusCode", 400);

			}

		} catch (Exception e) {
			response.put("message", e.getMessage());
			response.put("statusCode", 400);
		}

		return response;

	}

}
