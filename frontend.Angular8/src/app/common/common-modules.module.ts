import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../module/material-module';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { PopupModalComponent } from './popup-modal/popup-modal.component';
import { PopupDeleteComponent } from './popup-delete/popup-delete.component';
import { PopupLogoutComponent } from './popup-logout/popup-logout.component';
import { SweetAlertComponent } from './sweet-alert/sweet-alert.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { NgxSpinnerModule } from 'ngx-spinner';
import { ChartsModule } from 'ng2-charts';
import { BarchartComponent } from './barchart/barchart.component';
import { DoughnutComponent } from './doughnut/doughnut.component';
@NgModule({
  declarations: [
    PageLoaderComponent,
    PopupModalComponent,
    PopupDeleteComponent,
    PopupLogoutComponent,
    SweetAlertComponent,
    BarchartComponent,
    DoughnutComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    ChartsModule,

  ],
  exports: [
    PageLoaderComponent,
    PopupModalComponent,
    PopupDeleteComponent,
    PopupLogoutComponent,
    SweetAlertComponent,
    BarchartComponent,
    DoughnutComponent, FormsModule, ReactiveFormsModule
  ],
  providers: [
    NgbActiveModal
  ],
})
export class CommonModulesModule { }
