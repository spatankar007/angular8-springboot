import { Component, OnInit, HostBinding, ViewChild } from '@angular/core';

declare interface RouteInfo {
  route: string;
  label: string;
  icon: string;
  subMenu?: Array<Object>;
}
export const ROUTES: RouteInfo[] = [
  {
    route: '', label: 'Dashboard', icon: "dashboard", subMenu: [
      { label: "Home", route: "dashboard" },
      { label: "Setting", route: "setting" },
    ]
  },
  {
    route: 'table', label: 'Data Table', icon: "format_align_justify"
  },
  {
    route: '', label: 'User', icon: "inbox", subMenu: [
      { label: "Customer", route: "customer" },
      { label: "Inbox", route: "inbox" },
      { label: "Monetization", route: "chatroom" },
    ]
  }
];

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @ViewChild("popupLogout", { static: false }) popupLogout;
  @HostBinding('class.toggled')
  isOpen = false;
  hightlight = false;
  menuItems: any[];
  menuItemsIndex: number = -1;

  toggle() {
    this.isOpen = !this.isOpen;
  }
  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItems.map(i => {
      i.matchRoute = []
      if (i.subMenu) {
        i.subMenu.map(subI => {
          i.matchRoute.push(subI.route)
        });
      } else {
        i.matchRoute.push(i.route)
      }
    })
  }

  toggleSubMenu(index: number) {
    this.hightlight = !this.hightlight
    this.menuItemsIndex = this.menuItemsIndex == index ? -1 : index
  }

  indexValue() {
    return this.menuItemsIndex
  }

  logout() {
    this.popupLogout.open()
  }

}
