import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { UtilService } from '../../../service/util.service';
import { ConstantValues } from '../../../service/constants';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RoleRedirectionService } from '../../../service/roleRedirection.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('spinnerLoad', { static: false }) spinnerLoad;
  @ViewChild('modal', { static: false }) modal;
  loginForm: FormGroup;
  closeResult: string;
  submitted = false;
  constructor(
    private fb: FormBuilder,
    private constant: ConstantValues,
    private dataService: DataService,
    private utilService: UtilService,
    private router: Router,
    public roleRedirectionService: RoleRedirectionService,
    public toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      rememberMe: [true]
    });
    this.chkJWT();
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.valid) {
      localStorage.clear();
      this.spinnerLoad.spinnerShow();
      this.dataService.fakePost(this.loginForm.value).subscribe(res => {
        this.spinnerLoad.spinnerHide();
        this.toastrService.success('Welcome');
        localStorage.setItem('X-Auth-Token', this.constant.token);
        this.utilService.setLocal({ 'X-Auth-Role-Name': 'Admin' });
        this.roleRedirectionService.loginRedirection(this.utilService.getLocal(['X-Auth-Role-Name'])['X-Auth-Role-Name']);
      }, err => {
        this.spinnerLoad.spinnerHide();
        this.toastrService.error(err['message'])
      });
    }


  }

  chkJWT() {
    if (localStorage.getItem('X-Auth-Token')) {
      this.roleRedirectionService.loginRedirection(this.utilService.getLocal(['X-Auth-Role-Name'])['X-Auth-Role-Name']);
    }
  }

  checkBoxToggle() {
    this.loginForm.controls.rememberMe.setValue(!this.loginForm.controls.rememberMe.value)
  }
}