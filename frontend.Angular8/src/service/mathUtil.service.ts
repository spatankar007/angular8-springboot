import BigNumber from 'bignumber.js'
export class MathUtilService {
    static safeAdd(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.plus(y);

    }

    static safeSubtract(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.minus(y);
    }

    static safeDivide(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.dividedBy(y);
    }

    static safeMultiply(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.multipliedBy(y);
    }

    static valueOf(a) {
        return a.valueOf();

    }

    static isGreaterThan(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.isGreaterThan(y);

    }

    static isLessThan(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.isLessThan(y);

    }
    static isGreaterThanOrEqualTo(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.isGreaterThanOrEqualTo(y);

    }


    static isLessThanOrEqualTo(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.isLessThanOrEqualTo(y);

    }

    static toFixed(a, b) {
        let x = new BigNumber(a);
        return x.toFixed(b);

    }

    static toFixedNew(a, b) {
        let str = a.split(".");
        let str1 = str[0];
        let str2 = str[1];
        if (str2) {
            if (str2.length > b) {
                str2 = str2.slice(0, b);
            }
            else {
                let numofzero = b - str2.length;
                str2 += '0'.repeat(numofzero);
            }
        } else {
            str2 = '0000';
        }

        return str1 + '.' + str2;

    }

    static isEqualTo(a, b) {
        let x = new BigNumber(a);
        let y = new BigNumber(b);
        return x.isEqualTo(y);

    }
}