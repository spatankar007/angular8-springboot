// mymodal.component.ts
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataService } from '../../../service/data.service';
@Component({
  selector: 'app-popup-delete',
  templateUrl: './popup-delete.component.html',
  styleUrls: ['./popup-delete.component.scss']
})
export class PopupDeleteComponent implements OnInit {
  @ViewChild('mymodal', { static: false }) mymodal;
  @Input() module: string
  @Output() deleted = new EventEmitter()
  constructor(
    public modalService: NgbModal,
    private router: Router,
    private dataService: DataService,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrService
  ) { }

  deleteId = null

  ngOnInit() { }

  open(id) {
    this.deleteId = id
    this.modalService.open(this.mymodal, {
      centered: true,
      windowClass: 'popup-modal-delete'
    }).result.then((result) => {
      // Closed 
    }, (reason) => {
      // Dismissed
    });
  }

  delete() {
    this.closeOpened()
    /* this.spinner.show();
    this.dataService.deleteAPI(this.module, this.deleteId).subscribe(res => {
      if (res && res['success']) {
        this.spinner.hide();
        this.toastrService.success(res['message'])
        this.deleted.emit();
        this.deleteId = null
        this.closeOpened()
      }
      else {
        this.spinner.hide();
        this.toastrService.error(res['message'])
        this.deleted.emit();
        this.deleteId = null
        this.closeOpened()
      }
    }, err => {
      this.toastrService.error(err.error['message'])
      this.deleted.emit();
      this.deleteId = null
      this.closeOpened()
    }) */
  }

  closeOpened() {
    this.modalService.dismissAll()
  }
}
