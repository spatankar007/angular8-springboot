import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../service/data.service';
import { saveAs } from 'file-saver/dist/fileSaver';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  p: number = 1;
  collection: any[] = [
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
    'abc', 'abv', 'fbv', 'fsds',
  ]
  constructor(
    private dataService: DataService,
    public toastrService: ToastrService
  ) { }

  ngOnInit() { }

  ngAfterViewInit() { }

  download() {
    this.dataService.download('https://contractmanagement-file.s3.ap-south-1.amazonaws.com/CN_DIYD1203_12032020.pdf')
  }

  export(){
    this.dataService.modalExport('',this.collection)
  }

  pageChanged(event) {
    this.p = event;
  }

}
