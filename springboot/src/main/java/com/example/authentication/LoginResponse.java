package com.example.authentication;

public class LoginResponse {

	private Integer managerId;
	private String emailId;;
	private String password;

	public LoginResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginResponse(Integer managerId, String emailId, String password) {
		super();
		this.managerId = managerId;
		this.emailId = emailId;
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginResponse [managerId=" + managerId + ", emailId=" + emailId + ", password=" + password + "]";
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
