// mymodal.component.ts
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-popup-logout',
  templateUrl: './popup-logout.component.html',
  styleUrls: ['./popup-logout.component.scss']
})
export class PopupLogoutComponent implements OnInit {
  @ViewChild('mymodal', { static: false }) mymodal;
  constructor(
    public modalService: NgbModal,
    private router: Router,
    private toaster: ToastrService
  ) { }

  ngOnInit() { }

  open(content) {
    this.modalService.open(this.mymodal, {
      centered: true,
      windowClass: 'candidate-logout'
    }).result.then((result) => {
      // Closed 
    }, (reason) => {
      // Dismissed
    });
  }
  
  logout() {
    localStorage.clear();
    this.router.navigate(["/login"]);
    this.modalService.dismissAll();
    this.toaster.success("Sucessfully logout");
  }

  closeOpened() {
    this.modalService.dismissAll()
  }
}
