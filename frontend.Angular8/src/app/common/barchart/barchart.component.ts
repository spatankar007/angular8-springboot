import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.scss']
})
export class BarchartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public mbarChartLabels: string[] = ['2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartColors: Array<any> = [
    {
      backgroundColor: '#A4A1FB',
      borderColor: '#A4A1FB',
      pointBackgroundColor: '#A4A1FB',
      pointBorderColor: '#fafafa',
      pointHoverBackgroundColor: '#fafafa',
      pointHoverBorderColor: '#A4A1FB'
    },
    {
      backgroundColor: '#56D9FE',
      borderColor: '#56D9FE',
      pointBackgroundColor: '#56D9FE',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#56D9FE'
    },
    {
      backgroundColor: '#5FE3A1',
      borderColor: '#5FE3A1',
      pointBackgroundColor: '#5FE3A1',
      pointBorderColor: '#fafafa',
      pointHoverBackgroundColor: '#fafafa',
      pointHoverBorderColor: '#5FE3A1'
    }
  ];
  public barChartData: any[] = [
    { data: [55, 60, 75, 82, 56, 62, 80], label: 'Company A' },
    { data: [58, 55, 60, 79, 66, 57, 90], label: 'Company B' },
    { data: [55, 60, 75, 82, 56, 62, 80], label: 'Company C' }
  ];

  // events
  public chartClicked(e: any): void {
   
  }

  public chartHovered(e: any): void {
    
  }

  public randomize(): void {
    let data = [
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      Math.round(Math.random() * 100),
      (Math.random() * 100),
      Math.round(Math.random() * 100),
      (Math.random() * 100),
      Math.round(Math.random() * 100)];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
  }

}
