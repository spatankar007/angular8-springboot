import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpsService } from './https.service';
import { UtilService } from './util.service';
import { environment } from '../environments/environment';
import { ConstantValues } from './constants';
import { saveAs } from 'file-saver';
import { JsonToCsv } from './jsonToCsv';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  env: any;
  baseAPIUrl: string;
  fdAPIUrl: string;
  constructor(
    private httpsService: HttpsService,
    private utilService: UtilService,
    private constant: ConstantValues,
    private jsonToCsv: JsonToCsv,
  ) {
    this.env = environment;
    this.baseAPIUrl = this.env.apiUrl;
    this.fdAPIUrl = this.env.formDataUrl;
  }

  uploadHeaders: Object = {
    'content-type': undefined
  }

  download(url) {
    url = decodeURI(url)
    const ogName = this.utilService.getFileNameFromUrl(url)
    const extension = this.utilService.getFileExtension(url)
    const mimeType = this.constant.extensions.find(i => i[extension])[extension]
    this.httpsService.downloadGet(url).subscribe((res: any) => {
      if (res) {
        const blob = new Blob([res], { type: mimeType });
        const fileName = ogName;
        saveAs(blob, fileName);
      }
    })
  }

  modalExport(moduleName, data, type: string = 'excel') {
    if (data && !moduleName)
      this.jsonToCsv.exportExcel(data, 'sample')
    else
      this.export(moduleName).subscribe(res => {
        if (type == "csv")
          JsonToCsv.exportToCsv(moduleName, res['listData'])
        else
          this.jsonToCsv.exportExcel(res['listData'], moduleName)
      })
  }

  export(moduleName: any, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + `/${moduleName}/fetchAll`, null, headers);
  }

  /* Contract */

  fdPost(data?: any, headers?: any) {
    return this.httpsService.post(this.fdAPIUrl, data, null, this.uploadHeaders);
  }

  hrDashboardChart(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrdashboardchart', null, headers);
  }

  hrPipeLineStatus(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrpipelinestatus', null, headers);
  }

  deleteJobById(id, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrjobdelete', { 'jobId': id }, null, headers);
  }

  postContactlist(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/contactlist-post', data, null, headers);
  }

  getContactlist(userId, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrcontactlist-get?userid=' + userId, null, headers);
  }

  postCandidateReview(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/candidatereview', data, null, headers);
  }

  downloadFile(url?: string) {
    return this.httpsService.downloadGet(url);
  }

  getDashboard(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrdashboard', null, headers);
  }

  /* getJobList(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/joblist-get', null, headers);
  } */

  postJobList(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/joblist-post', data, null, headers);
  }

  hrLogin(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrlogin', data, null, headers);
  }

  fakeGet(id?: any, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/posts?userId=' + id, null, headers);
  }

  fakePost(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/posts', data, null, headers);
  }

  moreQuery(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrmorequery', data, null, headers);
  }

  candidateList(limit?: number, offset?: number, status?: string, headers?: any) {
    const statusUrl = status ? `&applicationStatus=${status}` : ''
    return this.httpsService.get(this.baseAPIUrl + '/candidatelist?offset=' + `${offset}` + statusUrl + '&limit=' + `${limit}`, null, headers);
  }


  candidateListall(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/candidatelist', null, headers);
  }

  getCandidateProfile(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hruserdetails?userid=' + encodeURIComponent(id), null, headers);
  }

  joiningDetails(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/joiningdetails-post', data, null, headers);
  }

  getjoiningDetails(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrjoiningdetails-get?userid=' + encodeURIComponent(id), null, headers);
  }

  contractLetter(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/contractletterhr-post', data, null, headers);
  }

  verify(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrdocupload-post', data, null, headers);
  }

  candidateDocumentRejection(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrrejectdocument', data, null, headers);

  }

  hrOfferLetterPost(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/offerletter-post', data, null, headers);
  }

  hrOtherInfoPost(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrotherinformation', data, null, headers);
  }

  //S3 fileupload re
  fileUpload(data?: any, headers?: any, fileName?: any) {
    const uploadHeaders = {
      'content-type': undefined,
      'type': headers,
      'filename': fileName
    }

    return this.httpsService.uploadPost(this.baseAPIUrl + '/hrfileupload', data, uploadHeaders, null);
  }

  hrUserUpdate(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hruserupadte', data, null, headers);
  }

  hrStagesUpdate(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrdashboardstagesupdate', data, null, headers);
  }

  hrSendEmail(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/hrsendemail', data, null, headers);
  }

  updateCandidate(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/updateapplicationstatus', data, null, headers);
  }

  getCandidateDocuments(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrdocumentget?userid=' + encodeURIComponent(id), null, headers);
  }

  postOnBoardStatus(data, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/onboardstatus', data, headers);
  }

  getMoreQuery(limit?: number, offset?: number, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/more-quereis-get?limit=' + limit + '&offSet=' + offset, null, headers);
  }

  getIdMoreQuery(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrmorequeriesquestion-get?queryId=' + id + '&status=get', null, headers);
  }

  deleteMoreQuery(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrmorequeriesquestion-get?queryId=' + id + '&status=delete', null, headers);
  }

  moreQueryCandidate(offset: number, limit: number, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrcontactqueries?limit=' + `${limit}` + '&offset=' + `${offset}`, null, headers);
  }

  getJobList(offset: number, limit: number, jobstatus: string, headers?: any) {
    const statusUrl = jobstatus ? `&jobstatus=${jobstatus}` : ''
    return this.httpsService.get(this.baseAPIUrl + '/joblist-get?limit=' + `${limit}` + statusUrl + '&offset=' + `${offset}`, null, headers);
  }

  getJobDetails(id: any, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/joblist-byid?jobid=' + id, null, headers);
  }

  postJoiningDetails() { }

  deleteJob(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/updatejobstatus?jobid=' + id, null, headers);
  }

  candidateSearch(id: any, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/candidatesearch?searchtext=' + id, null, headers);
  }

  searchJob(id: any, headers?: any) {
    return this.httpsService.get(
      this.baseAPIUrl + '/searchjob?searchtext=' + id, null, headers);
  }

  candidateQuery(id, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrcontactquerybyid?userid=' + encodeURIComponent(id), null, headers);
  }

  updateContactQuery(data?: any, headers?: any) {
    return this.httpsService.post(this.baseAPIUrl + '/updatecontactquery', data, null, headers);
  }

  recentQueries(headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrrecentqueries', null, headers);
  }

  contactQuerySearch(id: any, headers?: any) {
    return this.httpsService.get(this.baseAPIUrl + '/hrcontactouerysearch?searchtext=' + id, null, headers);
  }


  deleteContractandOfferLetter(uid, docToDelete, headers?: any) {
    const data = {
      userid: uid,
      docToDelete: docToDelete
    }
    return this.httpsService.post(this.baseAPIUrl + '/hrdeloffercontract', data, null, headers);
  }
}

