import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MessageService {
    private subject = new Subject<any>();
    private obj: string;

    sendMessage(message: any) {
        this.subject.next(message);
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }

    setData(obj: any , objName?:string) {
      if(objName){
        this.obj[`${objName}`] = obj;
      }else{

        this.obj = obj;
        }
        return true;
    }

    getData() {
        return this.obj;
    }

    removeData() {
        this.obj = null;
    }
}
