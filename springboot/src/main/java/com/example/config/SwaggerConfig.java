package com.example.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	// Stating the contact Information
	public static final Contact DEFAULT_CONTACT = new Contact("Saurabh Patankar", "https://www.google.com",
			"patankar74@gmail.com");

	// Document Information
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Angular-SpringBoot-API Documentation",
			"Angular-SpringBoot CRUD", "1.0", "urn:tos", DEFAULT_CONTACT, "Apache 2.0", // -API Documentation
			"http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());

	private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES = new HashSet<String>(
			Arrays.asList("application/json"));

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.example.springboot")).build();
	}

}
