import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';

import { CommonModulesModule } from '../../common/common-modules.module';

import { HomeComponent } from './home/home.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SidenavComponent } from '../../common/sidenav/sidenav.component';
import { NavbarComponent } from '../../common/navbar/navbar.component';
import { TableComponent } from './table/table.component';
import { ChartsModule } from 'ng2-charts';

import { NgxEchartsModule } from 'ngx-echarts';
import { CollapseModule, BsDropdownModule } from 'ngx-bootstrap';
import { InboxComponent } from './inbox/inbox.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { CustomerComponent } from './customer/customer.component';
import { ChatroomComponent } from './chatroom/chatroom.component';
import { HelpcenterComponent } from './helpcenter/helpcenter.component';
import { SettingsComponent } from './settings/settings.component';
import { CalendarComponent } from './calender/calendar.component';
import { ProfileComponent } from './profile/profile.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({

  declarations: [
    DashboardComponent,
    SidenavComponent,
    NavbarComponent,
    HomeComponent,
    TableComponent,
    InboxComponent,
    InvoiceComponent,
    CustomerComponent,
    ChatroomComponent,
    CalendarComponent,
    HelpcenterComponent,
    SettingsComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxEchartsModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    CommonModulesModule,
    ChartsModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    DashboardComponent,
    SidenavComponent,
    NavbarComponent,
    HomeComponent,
    TableComponent,
    InboxComponent,
    InvoiceComponent,
    CustomerComponent,
    ChatroomComponent,
    CalendarComponent,
    HelpcenterComponent,
    SettingsComponent,
    ProfileComponent
  ],
})
export class DashboardModule { }
