package com.example.employee;

import java.sql.Date;

public class EmployeeDetailsResponse {

	private Integer employeeId;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String roleName;
	private Date dateOfBirth;
	private String city;

	public EmployeeDetailsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmployeeDetailsResponse(Integer employeeId, String firstName, String lastName, String phoneNumber,
			String roleName, Date dateOfBirth, String city) {
		super();
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.roleName = roleName;
		this.dateOfBirth = dateOfBirth;
		this.city = city;
	}

	@Override
	public String toString() {
		return "EmployeeDetailsResponse [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName="
				+ lastName + ", phoneNumber=" + phoneNumber + ", roleName=" + roleName + ", dateOfBirth=" + dateOfBirth
				+ ", city=" + city + "]";
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
