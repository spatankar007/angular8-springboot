import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SweetModalService } from './modal.service';


@Injectable()
export class HttpsService {

  constructor(
    private http: HttpClient,
    private sweetModalService: SweetModalService,
    private router: Router,
  ) { }

  uploadPost(url: string, data: any, options?: Object, headers?: any) {
    let opts: Object = {}
    opts["headers"] = this.getRequestOptionArgs(headers);

    if (options) {
      opts = this.setHeaders(opts, options);
    }

    return this.http.post(url, data, opts).pipe(catchError((err: any) => this.handleError(err)));
  }

  post(url: string, data: any, options: any, headers?: any) {
    let opts: Object = {}
    opts["headers"] = this.getRequestOptionArgs(options);

    if (headers) {
      opts = this.setHeaders(opts, headers);
    }

    return this.http.post(url, data, opts).pipe(catchError((err: any) => this.handleError(err)));
    /* .catch((err) => {
      return this.handleError(err)
    }); */
  }

  put(url: string, data: any, options: any, headers?: any) {
    let opts: Object = {}
    opts["headers"] = this.getRequestOptionArgs(options);

    if (headers) {
      opts = this.setHeaders(opts, headers);
    }

    return this.http.put(url, data, opts).pipe(catchError((err: any) => this.handleError(err)));
  }

  get(url: string, options?: any, headers?: any) {
    let opts: Object = {}

    opts["headers"] = this.getRequestOptionArgs(options);

    if (headers) {
      opts = this.setHeaders(opts, headers);
    }

    return this.http.get(url, opts).pipe(catchError((err: any) => this.handleError(err)));
  }

  downloadGet(url: string) {
    let opts: Object = {}

    opts = { responseType: 'arraybuffer' };

    return this.http.get(url, opts).pipe(catchError((err: any) => this.handleError(err)));
  }

  delete(url: string, options: any) {
    return this.http.delete(url, { "headers": this.getRequestOptionArgs(options) });
  }

  setHeaders(options: any, headers: any) {
    if (!options || typeof headers !== "object") {
      return options;
    }

    for (const key in headers) {
      if (headers.hasOwnProperty(key)) {
        if (options["headers"].has(key)) {
          if (typeof headers[key] == 'string') {
            options["headers"] = options["headers"].delete(key);
            options["headers"] = options["headers"].set(key, headers[key]);
          } else {
            options["headers"] = options["headers"].delete(key);
          }
        } else {
          options["headers"] = options["headers"].set(key, headers[key]);
        }
      }
    }

    return options;
  }

  private getRequestOptionArgs(options?: HttpHeaders) {

    if (options == null) {
      options = new HttpHeaders();
    }
    options = options.set('Content-Type', "application/json");
    const userToken = localStorage.getItem('X-Auth-Token');
    // const userToken= false;

    if (userToken) {
      options = options.set('x-access-token', userToken);
    }
    return options;
  }

  //TODO catch error with handleError in each rest api call

  public handleError(error) {
    // check if user session is expired
    if (error.status === 401 || error.status === 403) {
      localStorage.clear();
      this.sweetModalService.openPopupMessage('Unauthorized error', 'Error')
      return this.router.navigate(['/']);
    }
    return throwError(error);
  }


}
