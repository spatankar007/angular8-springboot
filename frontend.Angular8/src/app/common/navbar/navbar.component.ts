import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, HostListener } from '@angular/core';
import { SidenavComponent } from '../sidenav/sidenav.component'
import { SweetModalService } from './../../../service/modal.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() sideBar: SidenavComponent;

  searchForm: FormGroup;

  isCollapsed = true;
  constructor(
    private fb: FormBuilder,
    public modalService: SweetModalService
  ) {
    this.searchForm = this.fb.group({
      search: ['', Validators.required]
    })
  }

  ngOnInit() {


  }

  @HostListener('click', ['$event.target.id'])
  click(event) {
    if (event == 'toggleMenu' || event == 'innerToggleMenu') {
      this.sideBar.toggle();
      this.isCollapsed = !this.isCollapsed;
    }

  }

  onSearch(form) {
    if (!form.valid) return;
  }
}
