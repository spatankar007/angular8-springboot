import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-sweet-alert',
  templateUrl: './sweet-alert.component.html',
  styleUrls: ['./sweet-alert.component.scss'],
  providers: [BsModalService]
})
export class SweetAlertComponent implements OnInit {
  @Input() message: string
  @Input() title: string
  @Output() action = new EventEmitter();
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {

  }

  okay() {
    this.action.emit(true)
    this.bsModalRef.hide()
  }

}
