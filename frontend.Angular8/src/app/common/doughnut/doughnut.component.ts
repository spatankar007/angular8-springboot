import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doughnut',
  templateUrl: './doughnut.component.html',
  styleUrls: ['./doughnut.component.scss']
})
export class DoughnutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

    public doughnutChartLabels:string[] = ['France', 'Itly', 'Spain','Germany'];
    public demodoughnutChartData:Array<number>[] = [[350, 300, 100,150]];
 
    public doughnutChartType:string = 'doughnut';
   
    // events
    public chartClicked(e:any):void {
      
    }
   
    public chartHovered(e:any):void {
      
    }

}
