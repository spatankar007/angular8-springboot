import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'
import { DataService } from '../../../service/data.service'
import { UtilService } from '../../../service/util.service'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  submitted = false;
  passcheck: boolean = true
  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      gender: ['', Validators.required],
      terms: [false, Validators.requiredTrue]
    });
  }

  fileChange(event) {
    this.f.userName.setValue(event.target.files[0])
  }

  get f() { return this.signupForm.controls; }

  onSubmit() {
    this.passcheck = true;
    this.submitted = true;
    if (this.f.password.value != this.f.confirmPassword.value) {
      this.passcheck = false;
    }
    if (this.signupForm.valid) {
      delete this.signupForm.value['userName']
      const formData = new FormData()
      formData.append('key1', this.f.userName.value);
      formData.append('key2', JSON.stringify(this.signupForm.value));
      this.dataService.fdPost(formData).subscribe(res => {
        this.utilService.goToLogin();
      })
      return;
    }

  }

  checkBoxToggle() {
    this.signupForm.controls.terms.setValue(!this.signupForm.controls.terms.value)
  }

}
