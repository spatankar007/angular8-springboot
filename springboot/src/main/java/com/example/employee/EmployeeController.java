package com.example.employee;

import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.util.JWTokenVerify;
import com.example.util.StatusMessage;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Value("${jwt.secret}")
	private String secret;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@SuppressWarnings({ "rawtypes" })
	@PostMapping("/create")
	public ResponseEntity<StatusMessage> saveUser(@RequestBody EmployeeModel employeeModel,
			@RequestHeader("access-token") String token) {
		StatusMessage statusMessage = new StatusMessage();

		try {
			// JWT token verification
			if (token != null) {
				// Method created to verify JWT token
				JSONObject responseObj = JWTokenVerify.verifyJWT(token, secret);
				if (responseObj.getString("message").equals("valid")) {
					EmployeeModel peopleModelResponse = employeeService.save(employeeModel);
					if (peopleModelResponse != null) {
						statusMessage.setMessage("Employee Added");
						statusMessage.setSuccess(true);
						return new ResponseEntity<>(statusMessage, HttpStatus.CREATED);
					} else {
						// return error message
						statusMessage.setMessage("Database error");
						statusMessage.setSuccess(false);
						return new ResponseEntity<>(statusMessage, HttpStatus.CREATED);
					}

				} else {
					statusMessage.setMessage("Missing token");
					statusMessage.setSuccess(false);
					return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);
				}
			} else {
				statusMessage.setMessage("missing token");
				statusMessage.setSuccess(false);
				return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);

			}

		} catch (Exception e) {
			statusMessage.setMessage(e.getMessage());
			statusMessage.setSuccess(false);
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
		}
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@PutMapping("/update/{id}")
	public ResponseEntity<StatusMessage> updateEmployee(@PathVariable(value = "id") Integer employeeId,
			@RequestBody EmployeeModel employeeModel, @RequestHeader("access-token") String token) {
		StatusMessage statusMessage = new StatusMessage();
		try {
			if (token != null) {
				// Method created to verify JWT token
				JSONObject responseObj = JWTokenVerify.verifyJWT(token, secret);
				if (responseObj.getString("message").equals("valid")) {
					EmployeeModel employee = employeeRepository.FindById(employeeId);
					if (employee != null) {
						employee.setLastName(employeeModel.getLastName());
						employee.setFirstName(employeeModel.getFirstName());
						employee.setAddress(employeeModel.getAddress());
						EmployeeModel updatedEmployee = employeeService.save(employee);
						statusMessage.setMessage("Updated");
						statusMessage.setSuccess(true);
						return new ResponseEntity<>(statusMessage, HttpStatus.OK);

					} else {
						// return error message
						statusMessage.setMessage("User not Found with Id" + " " + employeeId);
						statusMessage.setSuccess(false);
						return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);

					}

				} else {
					statusMessage.setMessage("Missing token");
					statusMessage.setSuccess(false);
					return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);
				}
			} else {
				statusMessage.setMessage("missing token");
				statusMessage.setSuccess(false);
				return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);

			}

		} catch (Exception e) {
			statusMessage.setMessage(e.getMessage());
			statusMessage.setSuccess(false);
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
		}
	}

	@SuppressWarnings("rawtypes")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<StatusMessage> deleteEmployee(@PathVariable(value = "id") Integer employeeId,
			@RequestHeader("access-token") String token) {
		StatusMessage statusMessage = new StatusMessage();
		try {
			if (token != null) {
				// Method created to verify JWT token
				JSONObject responseObj = JWTokenVerify.verifyJWT(token, secret);
				if (responseObj.getString("message").equals("valid")) {
					EmployeeModel employee = employeeRepository.FindById(employeeId);
					if (employee != null) {
						employeeRepository.delete(employee);
						statusMessage.setMessage("Deleted");
						statusMessage.setSuccess(true);
						return new ResponseEntity<>(statusMessage, HttpStatus.OK);
					} else {
						// return error message
						statusMessage.setMessage("User not Found with Id" + " " + employeeId);
						statusMessage.setSuccess(false);
						return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);

					}

				} else {
					statusMessage.setMessage("Missing token");
					statusMessage.setSuccess(false);
					return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);
				}
			} else {
				statusMessage.setMessage("missing token");
				statusMessage.setSuccess(false);
				return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);

			}

		} catch (Exception e) {
			statusMessage.setMessage(e.getMessage());
			statusMessage.setSuccess(false);
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/fetchAll")
	public ResponseEntity<StatusMessage> fetchAll(@RequestHeader("access-token") String token) {
		StatusMessage statusMessage = new StatusMessage();

		try {
			// ** JW Token Verification
			if (token != null) {
				// Method created to verify JWT token
				JSONObject responseObj = JWTokenVerify.verifyJWT(token, secret);
				if (responseObj.getString("message").equals("valid")) {

					List<EmployeeModel> response = employeeRepository.findAll();
					if (!(response.isEmpty())) {
						statusMessage.setMessage("Fetch success");
						statusMessage.setListData(response);
						statusMessage.setSuccess(true);
						return new ResponseEntity<>(statusMessage, HttpStatus.OK);

					} else {
						statusMessage.setMessage("data Not found");
						statusMessage.setListData(response);
						statusMessage.setSuccess(false);

						return new ResponseEntity<>(statusMessage, HttpStatus.OK);
					}
				} else {
					statusMessage.setMessage("Missing token");
					statusMessage.setSuccess(false);
					return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);
				}
			} else {
				statusMessage.setMessage("missing token");
				statusMessage.setSuccess(false);
				return new ResponseEntity<>(statusMessage, HttpStatus.UNAUTHORIZED);

			}
		} catch (Exception e) {
			statusMessage.setMessage(e.getMessage());
			statusMessage.setSuccess(false);
			logger.info("Exception: " + e.getMessage());
			return new ResponseEntity<>(statusMessage, HttpStatus.BAD_REQUEST);
		}
	}
}
